function affiche(i) {
    $('#titre').html(exemples[i].nom);
    $.ajax({
        mimeType: 'text/plain;',
        url: 'assets/' + exemples[i].python,
        dataType: "text"
    }).done(function (data) {
        $('#code').html(data);
        document.querySelectorAll('#code').forEach(function (block) {
            hljs.highlightBlock(block);
            hljs.lineNumbersBlock(block);
        });
    });
    $.ajax({
        mimeType: 'text/html',
        url: 'assets/' + exemples[i].comment,
        dataType: "html"
    }).done(function (data) {
        $('#comment').html(data);
        document.querySelectorAll('#comment pre code').forEach(function (block) {
            hljs.highlightBlock(block);
        });
    });
}

function initialise() {
    let n = 0
    exemples.forEach(function (e, i) {
        if (e.tags.includes('title')) {
            lien = "<span class=\"separators\">";
            lien += e.nom;
            lien += "</span>";
        } else {
            n = n + 1;
            lien = "<a class=\"dropdown-item\" href=\"#\" onclick=\"";
            lien += "affiche(" + i + ")";
            lien += "\">" + n + ". " + e.nom + "</a>";
        }
        $('#listeExemples').append(lien);
    });
    affiche(1);
}