#!/usr/bin/python3

def position_minimum(liste):
    indice_min = 0
    for indice in range(len(liste)):
        if liste[indice] < liste[indice_min]:
            indice_min = indice
    return indice_min
