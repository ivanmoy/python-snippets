#!/usr/bin/python3

def minimum(liste):
    mini = liste[0]
    for element in liste:
        if element < mini :
            mini = element
    return mini
