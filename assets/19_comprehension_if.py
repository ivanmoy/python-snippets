#!/usr/bin/python3

nombres = [-3,5,7,-6,14]

# Méthode usuelle
positifs = []
for n in nombres :
    if n >=0 :
        positifs.append(n)

# Comprehension
positifs = [n for n in nombres if n >=0]