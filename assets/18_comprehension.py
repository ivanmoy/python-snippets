#!/usr/bin/python

nombres = [2,4,9,3]

# methode usuelle
carres = []
for n in nombres :
    carres.append(n**2)

# comprehension
carres = [n**2 for n in nombres]